package ru.education.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.education.security.SecurityUserDetailsManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import static io.jsonwebtoken.lang.Strings.hasText;

@Service
public class TokenAuthenticationService {
    private static final String SECRET = "Secret";
    private static final long EXPIRATION_TIME = 10_000;
    private static final String TOKEN_PREFIX = "BEARER";
    private static final String HEADER_STRING = "Authentication";

    private static Logger logger = LoggerFactory.getLogger(TokenAuthenticationService.class);

    private static SecurityUserDetailsManager securityUserDetailsManager;

    public TokenAuthenticationService(SecurityUserDetailsManager securityUserDetailsManager) {
        TokenAuthenticationService.securityUserDetailsManager = securityUserDetailsManager;
    }

    static void addAuthentication(HttpServletResponse response, String userName) {
        response.setHeader(HEADER_STRING, TOKEN_PREFIX + " " + generateToken(userName));
    }

    static Authentication getAuthentication(HttpServletRequest request){
        String token = getToken(request);
        if(!hasText(token)){
            return null;
        }

        //проверка наличий сессий по токену

        String userName = getUserName(token);
        UserDetails user = securityUserDetailsManager.loadUserByUsername(userName);
        if(user == null)
            return null;
        return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
    }

    private static String generateToken(String userName){
        return Jwts.builder()
                .setSubject(userName)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    private static String getToken(HttpServletRequest request){
        if(request.getHeader(HEADER_STRING) != null){
            return request.getHeader(HEADER_STRING).replace(TOKEN_PREFIX + " ", "");
        } else {
            return null;
        }
    }

    public static String getUserName(String token){
        try {
            return token!= null ? Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject() : null;
        } catch (ExpiredJwtException e) {
            logger.info("Ошибка обработки токена - токен просрочен: {}", token);
            return null; //Токен просрочен
        } catch (JwtException e){
            logger.info("Ошибка обработки токена: {}", token);
            return null; //ошибкка обработки токена
        }
    }
}
