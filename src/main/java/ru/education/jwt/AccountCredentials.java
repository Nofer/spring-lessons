package ru.education.jwt;

import lombok.Getter;

@Getter
public class AccountCredentials {

    private String username;
    private String password;

}
