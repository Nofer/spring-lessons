package ru.education.author;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import ru.education.book.Book;
import ru.education.book.BookRepository;
import ru.education.exceptions.EntityAlreadyExistsExceprion;
import ru.education.exceptions.EntityHasDetailException;
import ru.education.exceptions.EntityIllegalArgException;
import ru.education.exceptions.EntityNotFoundException;
import ru.education.util.ParamTransformer;

import java.util.List;

@Primary
@Service("authorService")
public class DefaultAuthorService implements AuthorService {
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    public DefaultAuthorService(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    public List<Author> findAll() {
        return authorRepository.findAll();
    }


    public Author findById(Object id) {
        Long parseId = ParamTransformer.getIdFromObj(id);
        Author author = authorRepository.findById(parseId).orElse(null);

        if (author == null) {
            throw new EntityNotFoundException(Author.TYPE_NAME, parseId);
        }

        return author;
    }

    public Author create(Author author) {
        if (author == null) {
            throw new EntityIllegalArgException("Создаваемый объект не может быть равен null");
        }
        if (author.getId() == null) {
            throw new EntityIllegalArgException("Ид объекта не может быть равен null");
        }
        Author existAuthor = authorRepository.findById(author.getId()).orElse(null);
        if (existAuthor != null) {
            throw new EntityAlreadyExistsExceprion(Author.TYPE_NAME, author.getId());
        }
        return authorRepository.save(author);
    }

    public Author update(Author author) {
        if (author == null) {
            throw new EntityIllegalArgException("Создаваемый объект не может быть равен null");
        }
        if (author.getId() == null) {
            throw new EntityIllegalArgException("Ид объекта не может быть равен null");
        }
        Author existAuthor = authorRepository.findById(author.getId()).orElse(null);
        if (existAuthor == null) {
            throw new EntityNotFoundException(Author.TYPE_NAME, author.getId());
        }
        return authorRepository.save(author);
    }

    public void delete(Object id) {
        Author author = findById(id);
        List<Book> authorBooks = bookRepository.getBooksByAuthor(author);
        if (authorBooks.size() > 0) {
            throw new EntityHasDetailException(Book.TYPE_NAME, id);
        }
        authorRepository.deleteById(author.getId());
    }
}
