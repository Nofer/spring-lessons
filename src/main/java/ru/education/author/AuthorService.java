package ru.education.author;

import java.util.List;

public interface AuthorService {

    List<Author> findAll();

    Author findById(Object id);

    Author create(Author author);

    Author update(Author author);

    void delete(Object id);
}
