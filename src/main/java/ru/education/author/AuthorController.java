package ru.education.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/authors")
public class AuthorController {

    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    @PreAuthorize("hasPermission('author', 'read')")
    public List<Author> getProducts() {
        return authorService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasPermission('author', 'read')")
    public Author getById(@PathVariable String id) {
        return authorService.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasPermission('author', 'edit')")
    public Author create(@RequestBody Author author) {
        return authorService.create(author);
    }

    @PutMapping
    @PreAuthorize("hasPermission('author', 'edit')")
    public Author update(@RequestBody Author author) {
        return authorService.update(author);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasPermission('author', 'edit')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        authorService.delete(id);
    }
}
