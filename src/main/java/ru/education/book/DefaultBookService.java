package ru.education.book;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import ru.education.author.Author;
import ru.education.author.AuthorRepository;
import ru.education.exceptions.EntityAlreadyExistsExceprion;
import ru.education.exceptions.EntityIllegalArgException;
import ru.education.exceptions.EntityNotFoundException;
import ru.education.util.ParamTransformer;

import java.util.List;

@Primary
@Service("bookService")
public class DefaultBookService implements BookService{
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    public DefaultBookService(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    public List<Book> findAll() {
        return bookRepository.findAll();
    }


    public Book findById(Object id) {
        Long parseId = ParamTransformer.getIdFromObj(id);
        Book book = bookRepository.findById(parseId).orElse(null);

        if (book == null) {
            throw new EntityNotFoundException(Book.TYPE_NAME, parseId);
        }

        return book;
    }

    public Book create(Book book) {
        checkBook(book);
        Book existBook = bookRepository.findById(book.getId()).orElse(null);
        if (existBook != null) {
            throw new EntityAlreadyExistsExceprion(Book.TYPE_NAME, book.getId());
        }
        return bookRepository.save(book);
    }

    public Book update(Book book) {
        checkBook(book);
        Book existBook = bookRepository.findById(book.getId()).orElse(null);
        if (existBook == null) {
            throw new EntityNotFoundException(Book.TYPE_NAME, book.getId());
        }
        return bookRepository.save(book);
    }

    public void delete(Object id) {
        Book book = findById(id);
        bookRepository.deleteById(book.getId());
    }

    public List<Book> getBooksByAuthor(Object authorId) {
        if (authorId == null) {
            throw new EntityIllegalArgException("Ид автора не может быть равен null");
        }
        Long parseId = ParamTransformer.getIdFromObj(authorId);
        List<Book> bookList = bookRepository.getBooksByAuthor(new Author(parseId, null));
        if (bookList.size() == 0) {
            throw new EntityNotFoundException(String.format("Не найдены книги по ключу автора %s", authorId));
        }
        return bookList;
    }

    private void checkBook(Book book){
        if (book == null) {
            throw new EntityIllegalArgException("Создаваемый объект не может быть равен null");
        }
        if (book.getId() == null || book.getTitle() == null || book.getYear() == null) {
            throw new EntityIllegalArgException("Ид, название или дата создания книги не может быть равна null");
        }
        if (book.getAuthor() == null) {
            throw new EntityIllegalArgException("Автор не может быть равен null");
        }
        Long authorId = book.getAuthor().getId();
        if (authorId == null) {
            throw new EntityIllegalArgException("Ид автора не может быть равен null");
        }
        Author author = authorRepository.findById(authorId).orElse(null);
        if (author == null) {
            throw new EntityNotFoundException(Author.TYPE_NAME, authorId);
        }
    }
}
