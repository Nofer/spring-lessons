package ru.education.book.jdbc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class BookJDBC {
    private long id;
    private String title;
    private Date year;
    private long authorId;
}
