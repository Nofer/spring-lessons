package ru.education.book.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookJdbcRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public BookJdbcRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int count() {
        return jdbcTemplate.queryForObject("select count(*) from book", Integer.class);
    }

    public List<BookJDBC> getAll(){
        return jdbcTemplate.query("select * from book", (rs, rowNum) ->
                new BookJDBC(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getDate("year"),
                        rs.getLong("author_id")
                ));
    }

    public int save(BookJDBC book){
        return jdbcTemplate.update("insert into book values (?, ?, ?, ?)",
                book.getId(),
                book.getTitle(),
                book.getYear(),
                book.getAuthorId()
                );
    }
}
