package ru.education.book;

import java.util.List;

public interface BookService {

    List<Book> findAll();

    Book findById(Object id);

    Book create(Book author);

    Book update(Book author);

    void delete(Object id);

    List<Book> getBooksByAuthor(Object id);
}
