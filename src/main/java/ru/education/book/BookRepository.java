package ru.education.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.education.author.Author;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

//    @Query(value = "select max(price) from sales_product where product=productId", nativeQuery = true)
//    Integer getMaxPriceByProductId(@Param("productId") long productId);
//

    List<Book> getBooksByAuthor(Author author);

}
