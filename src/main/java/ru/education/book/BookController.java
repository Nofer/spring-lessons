package ru.education.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/books")
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    @PreAuthorize("hasPermission('book', 'read')")
    public List<Book> getAll() {
        return bookService.findAll();
    }


    @GetMapping("/{id}")
    @PreAuthorize("hasPermission('book', 'read')")
    public Book getBooksById(@PathVariable Object id) {
        return bookService.findById(id);
    }

    @GetMapping("/author/{id}")
    @PreAuthorize("hasPermission('book', 'read')")
    public List<Book> getBooksByAuthorId(@PathVariable Object id) {
        return bookService.getBooksByAuthor(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasPermission('book', 'edit')")
    public Book save(@RequestBody Book book) {
        return bookService.create(book);
    }

    @PutMapping
    @PreAuthorize("hasPermission('book', 'edit')")
    public Book update(@RequestBody Book book) {
        return bookService.update(book);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasPermission('book', 'edit')")
    public void delete(@PathVariable Object id) {
        bookService.delete(id);
    }


//    @RequestMapping("/books/jdbc")
//    public List<BookJDBC> getAllJdbc() {
//        return bookJdbcRepository.getAll();
//    }
//
//    @RequestMapping("/save/jdbc")
//    public int save(@RequestBody BookJDBC book) {
//        return bookJdbcRepository.save(book);
//    }

}
