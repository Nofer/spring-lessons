package ru.education.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class SecurityUserDetailsManager implements UserDetailsManager {

    @Override
    public void createUser(UserDetails userDetails) {

    }

    @Override
    public void updateUser(UserDetails userDetails) {

    }

    @Override
    public void deleteUser(String s) {

    }

    @Override
    public void changePassword(String s, String s1) {

    }

    @Override
    public boolean userExists(String s) {
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        //логика по вычитке пользователя
        if(userName == null)
            return null;
        switch (userName){
            case "admin":
                return new SecurityUser(userName, "{noop}qwe", Collections.singletonList(new SecurityPermission("admin")));
            default: {
                List<SecurityPermission> permissions = new ArrayList<>();
                permissions.add(new SecurityPermission("author.read"));
                return new SecurityUser(userName, "{noop}123", permissions);
            }
        }
    }
}
