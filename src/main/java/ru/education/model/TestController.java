package ru.education.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1")
public class TestController {

    private final Formatter formatter;


    @Autowired
    public TestController(@Qualifier("fooFormatter") Formatter formatter) {
        this.formatter = formatter;
    }

    @RequestMapping("/hello")
    public String getHello() {
        return "Hello";
    }

    @RequestMapping("/format")
    public String getFormat() {
        return formatter.format();
    }

}
