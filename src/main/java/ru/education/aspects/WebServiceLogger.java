package ru.education.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class WebServiceLogger {

    private static Logger logger = LoggerFactory.getLogger(WebServiceLogger.class);

    @Pointcut(value = "execution(public * ru.education.author.AuthorService.*(..))")
    public void authorServiceMethod(){}

    @Pointcut(value = "execution(public * ru.education.book.BookService.*(..))")
    public void bookServiceMethod(){}

    @Pointcut("@annotation(ru.education.annotation.Loggable)")
    public void loggableMethod(){}

    @Before(value = "authorServiceMethod() || bookServiceMethod()")
    public void logWebServiceCallBefore(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        Object[] methodArgs = joinPoint.getArgs();
        logger.info("Call method " + methodName + " with args " + Arrays.toString(methodArgs));
    }

    @AfterReturning(value = "authorServiceMethod() || bookServiceMethod()", returning = "result")
    public void logWebServiceCallReturn(JoinPoint joinPoint, Object result) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("Method " + methodName + " returns " + result);
    }

    @AfterThrowing(value = "authorServiceMethod() || bookServiceMethod()", throwing = "ex")
    public void logWebServiceCallError(JoinPoint joinPoint, Throwable ex) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("Method " + methodName + " throw " + ex.getMessage());
    }

    @After(value = "authorServiceMethod() || bookServiceMethod()")
    public void logWebServiceCallAfter(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        logger.info("Method " + methodName + " was called");
    }

}
