package ru.education.util;

import ru.education.exceptions.EntityIllegalArgException;

public class ParamTransformer {

    public static Long getIdFromObj (Object id){
        if (id == null) {
            throw new EntityIllegalArgException("Ид объекта не может быть равен null");
        }
        try {
            return Long.parseLong((id.toString()));
        } catch (NumberFormatException e) {
            throw new EntityIllegalArgException(String.format("Не удалось преобразовать ид к " +
                    "нужному типу, текст ошибки: %s", e.getMessage()));
        }
    }
}
