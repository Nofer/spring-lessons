package ru.education.exceptions;

/**
 * вызов метода сервиса с неправильными параметрами
 */
public class EntityIllegalArgException extends BaseException {

    public EntityIllegalArgException(String message) {
        super(message);
    }
}
