package ru.education.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
class ErrorResponseEntity {

    private String message;
    private String error;
    private int status;
}
