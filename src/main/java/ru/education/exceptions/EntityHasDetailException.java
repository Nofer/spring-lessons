package ru.education.exceptions;

import org.springframework.util.Assert;

/**
 * Удаление сущности, на которую есть ссылки у других сущностей
 */
public class EntityHasDetailException extends BaseException {

    public EntityHasDetailException(String message) {
        super(message);
    }

    public EntityHasDetailException(String type, Object id) {
        this(formatMessage(type, id));
    }

    private static String formatMessage(String type, Object id) {
        Assert.hasText(type, "Тип не может быть пустым");
        Assert.notNull(id, "Ид объекта не может быть null");
        Assert.hasText(id.toString(), "Ид не может быть пустым");
        return String.format("%s ссылается на удаляемый объект с id %s", type, id);
    }
}
