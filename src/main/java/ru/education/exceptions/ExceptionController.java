package ru.education.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionController {

    private static ErrorResponseEntity createErrorResponseEntity(BaseException e, HttpStatus status){
        return new ErrorResponseEntity(e.getMessage(), status.getReasonPhrase(), status.value());
    }

    @ExceptionHandler(EntityIllegalArgException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponseEntity handleIllegalArgException(EntityIllegalArgException ex){
        return  createErrorResponseEntity(ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityAlreadyExistsExceprion.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public ErrorResponseEntity handleAlreadyExistsExceprion(EntityAlreadyExistsExceprion ex){
        return  createErrorResponseEntity(ex, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EntityConflictException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public ErrorResponseEntity handleConflictException(EntityConflictException ex){
        return  createErrorResponseEntity(ex, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EntityHasDetailException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public ErrorResponseEntity handleHasDetailException(EntityHasDetailException ex){
        return  createErrorResponseEntity(ex, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorResponseEntity handleNotFoundException(EntityNotFoundException ex){
        return  createErrorResponseEntity(ex, HttpStatus.NOT_FOUND);
    }
}
