insert into author values (1, 'Пушкин');
insert into author values (2, 'Толстой');
insert into author values (3, 'Автор без книг');

insert into book values (1, 'Сказки Пушкина', '1984-03-21', 1);
insert into book values (2, 'Биография Толстого', '1985-03-21', 2);

SELECT nextval('book_id_seq');
SELECT nextval('book_id_seq');