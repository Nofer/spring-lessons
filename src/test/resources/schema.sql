DROP table if EXISTS book;
DROP table if EXISTS author;
DROP SEQUENCE  if EXISTS book_id_seq;

create sequence book_id_seq;

create table author
(
    id   bigint not null
        constraint users_pkey
            primary key,
    name text   not null
);
create table book
(
    id        bigserial not null
        constraint book_pk
            primary key,
    title     text      not null,
    year      date      not null,
    author_id bigint    not null
        constraint book_author_id_fk
            references author
);
