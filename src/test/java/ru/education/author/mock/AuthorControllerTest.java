package ru.education.author.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.education.author.Author;
import ru.education.author.AuthorController;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AuthorController.class, MockAuthorService.class})
public class AuthorControllerTest {

    @Autowired
    private AuthorController authorController;
    private MockMvc mockMvc;
    private final static String URL = "http://localhost:8080/api/v1/authors";
    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setup(){
        this.mockMvc = standaloneSetup(authorController).build();
    }

    @Test
    public void findAll() throws Exception {
        mockMvc.perform(get(URL)).andExpect(status().isOk());
    }

    @Test
    public void finById() throws Exception {
        mockMvc.perform(get(URL + "/1")).andExpect(status().isOk());
    }

    @Test
    public void create() throws Exception {
        Author author = new Author(1L, "qwe");
        String requestJson = mapper.writeValueAsString(author);
        mockMvc.perform(post(URL).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void update() throws Exception {
        Author author = new Author(1L, "qwe");
        String requestJson = mapper.writeValueAsString(author);
        mockMvc.perform(put(URL).contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    public void remove() throws Exception {
        mockMvc.perform(delete(URL + "/1")).andExpect(status().isNoContent());
    }
}
