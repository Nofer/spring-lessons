package ru.education.author.mock;

import org.springframework.stereotype.Service;
import ru.education.author.Author;
import ru.education.author.AuthorService;

import java.util.ArrayList;
import java.util.List;

@Service("mock")
public class MockAuthorService implements AuthorService {

    @Override
    public List<Author> findAll() {
        return new ArrayList<>();
    }

    @Override
    public Author findById(Object id) {
        return new Author();
    }

    @Override
    public Author create(Author author) {
        return author;
    }

    @Override
    public Author update(Author author) {

        return author;
    }

    @Override
    public void delete(Object id) {
    }
}
