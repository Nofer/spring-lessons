package ru.education.author;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {AuthorTestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class AuthorRepositoryTest {

    @Autowired
    private AuthorRepository authorRepository;

    @Test
    public void save() {
        Author author = new Author(4L, "Автор");
        authorRepository.save(author);
        Author authorSaved = authorRepository.findById(author.getId()).orElse(null);
        Assert.assertNotNull(authorSaved);
    }

    @Test
    public void getById() {
        Author author = authorRepository.findById(1L).orElse(null);
        Assert.assertNotNull(author);
    }


    @Test
    public void delete() {
        Long authorDeletedId = 3L;
        authorRepository.deleteById(authorDeletedId);
        Author authorSaved = authorRepository.findById(authorDeletedId).orElse(null);
        Assert.assertNull(authorSaved);
    }

}