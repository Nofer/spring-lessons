package ru.education.author;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.education.exceptions.EntityAlreadyExistsExceprion;
import ru.education.exceptions.EntityHasDetailException;
import ru.education.exceptions.EntityIllegalArgException;
import ru.education.exceptions.EntityNotFoundException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {AuthorTestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class AuthorServiceTest {

    @Autowired
    private DefaultAuthorService authorService;

    @Test
    void findAll() {
        List<Author> authorList = authorService.findAll();
        assertEquals(authorList.size(), 3);
    }

    @Test
    void findByNullId() {
        assertThrows(EntityIllegalArgException.class, () -> authorService.findById(null));
    }

    @Test
    void findByIllegalId() {
        assertThrows(EntityIllegalArgException.class, () -> authorService.findById("id"));
    }

    @Test
    void findByIdNotFound() {
        assertThrows(EntityNotFoundException.class, () -> authorService.findById(5L));
    }

    @Test
    void findById() {
        assertNotNull(authorService.findById(1L));
    }

    @Test
    void createNullAuthor() {
        assertThrows(EntityIllegalArgException.class, () -> authorService.create(null));
    }

    @Test
    void createAuthorNullId() {
        assertThrows(EntityIllegalArgException.class, () -> authorService.create(new Author(null, "test")));
    }

    @Test
    void createExistAuthor() {
        assertThrows(EntityAlreadyExistsExceprion.class, () -> authorService.create(new Author(1L, "test")));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void create() {
        Author author = new Author(4L, "test");
        assertNotNull(authorService.create(author));
    }

    @Test
    void deleteWithDetail() {
        assertThrows(EntityHasDetailException.class, () -> authorService.delete(1L));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void delete() {
        Long authorDeletedId = 3L;
        authorService.delete(authorDeletedId);
        assertThrows(EntityNotFoundException.class, () -> authorService.findById(authorDeletedId));
    }
}