package ru.education.author;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = {"ru.education.author", "ru.education.book"})
@ComponentScan(basePackages = {"ru.education.author"})
@EntityScan(basePackages = {"ru.education.author", "ru.education.book"})
public class AuthorTestConfig {
}
