package ru.education.book;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = {"ru.education.book", "ru.education.author"})
@ComponentScan(basePackages = {"ru.education.book"})
@EntityScan(basePackages = {"ru.education.author", "ru.education.book"})
public class BookTestConfig {
}
