package ru.education.book;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.education.author.Author;

import java.util.Date;


@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {BookTestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class BookRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @Test
    public void getById() {
        Book book = bookRepository.findById(1L).orElse(null);
        Assert.assertNotNull(book);
    }

    @Test
    public void save() {
        Book book = new Book();
        book.setId(3L);
        book.setTitle("Третья книга");
        book.setYear(new Date());
        book.setAuthor(new Author(1L, "test"));
        bookRepository.save(book);
        Book bookSaved = bookRepository.findById(book.getId()).orElse(null);
        Assert.assertNotNull(bookSaved);
    }

    @Test
    public void delete() {
        Long bookDeletedId = 2L;
        bookRepository.deleteById(bookDeletedId);
        Book bookSaved = bookRepository.findById(bookDeletedId).orElse(null);
        Assert.assertNull(bookSaved);
    }

}