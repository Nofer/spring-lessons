package ru.education.book;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.education.author.Author;
import ru.education.exceptions.EntityAlreadyExistsExceprion;
import ru.education.exceptions.EntityIllegalArgException;
import ru.education.exceptions.EntityNotFoundException;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {BookTestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class BookServiceTest {

    @Autowired
    private DefaultBookService bookService;

    @Test
    void findAll() {
        List<Book> bookList = bookService.findAll();
        assertEquals(bookList.size(), 2);
    }

    @Test
    void findByNullId() {
        assertThrows(EntityIllegalArgException.class, () -> bookService.findById(null));
    }

    @Test
    void findByIllegalId() {
        assertThrows(EntityIllegalArgException.class, () -> bookService.findById("id"));
    }

    @Test
    void findByIdNotFound() {
        assertThrows(EntityNotFoundException.class, () -> bookService.findById(5L));
    }

    @Test
    void findById() {
        assertNotNull(bookService.findById(1L));
    }

    @Test
    void createNullBook() {
        assertThrows(EntityIllegalArgException.class, () -> bookService.create(null));
    }

    @Test
    void createBookNullId() {
        assertThrows(EntityIllegalArgException.class, () -> bookService.create(new Book()));
    }

    @Test
    void createBookNullAuthor() {
        Book book = new Book();
        book.setId(3L);
        assertThrows(EntityIllegalArgException.class, () -> bookService.create(book));
    }

    @Test
    void createBookNullAuthorId() {
        Book book = new Book();
        book.setId(3L);
        book.setAuthor(new Author());
        assertThrows(EntityIllegalArgException.class, () -> bookService.create(book));
    }

    @Test
    void createBookNotExistAuthor() {
        Book book = new Book();
        book.setId(3L);
        book.setTitle("title");
        book.setYear(new Date());
        book.setAuthor(new Author(4L, "test"));
        assertThrows(EntityNotFoundException.class, () -> bookService.create(book));
    }

    @Test
    void createExistBook() {
        Book book = new Book();
        book.setId(1L);
        book.setTitle("title");
        book.setYear(new Date());
        book.setAuthor(new Author(1L, "test"));
        assertThrows(EntityAlreadyExistsExceprion.class, () -> bookService.create(book));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void create() {
        Book book = new Book();
        book.setId(4L);
        book.setTitle("title");
        book.setYear(new Date());
        book.setAuthor(new Author(1L, "test"));
        assertNotNull(bookService.create(book));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    void delete() {
        Long bookDeletedId = 2L;
        bookService.delete(bookDeletedId);
        assertThrows(EntityNotFoundException.class, () -> bookService.findById(bookDeletedId));
    }

    @Test
    void getBooksByAuthorNullId() {
        assertThrows(EntityIllegalArgException.class, () -> bookService.getBooksByAuthor(null));
    }

    @Test
    void getBooksByNoExistAuthor() {
        assertThrows(EntityNotFoundException.class, () -> bookService.getBooksByAuthor(4L));
    }

    @Test
    void getBooksByAuthor() {
        List<Book> booksList = bookService.getBooksByAuthor(1L);
        assertEquals(booksList.size(), 1);
    }
}