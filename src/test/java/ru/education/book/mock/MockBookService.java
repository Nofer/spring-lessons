package ru.education.book.mock;

import org.springframework.stereotype.Service;
import ru.education.book.Book;
import ru.education.book.BookService;

import java.util.ArrayList;
import java.util.List;

@Service("mock")
public class MockBookService implements BookService {


    @Override
    public List<Book> findAll() {
        return new ArrayList<>();
    }

    @Override
    public Book findById(Object id) {
        return new Book();
    }

    @Override
    public Book create(Book author) {
        return author;
    }

    @Override
    public Book update(Book author) {
        return author;
    }

    @Override
    public void delete(Object id) {

    }

    @Override
    public List<Book> getBooksByAuthor(Object id) {
        return new ArrayList<>();
    }
}
